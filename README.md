# Node JSON Kata

Kata instructions : https://github.com/planity/test_recrutement/tree/master/backend_junior

## Installation

Install the project using npm : 
```bash
npm i
```

## Running the script

First, copy the .sample.env file as .env:
```bash
cp .sample.env .env
```

Then write your JSONBIN master key as the variable JSONBIN_API_KEY in the .env file.

Then run the script with node : 
```bash
node src/index.js
```

## Running tests

Run tests with npm : 
```bash
npm test
```