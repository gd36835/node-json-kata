import { compileAndSaveEndpoints, BASEPATH, mergeIgnoringNull, capitalizeFirstLetter, sanitizeName } from "../src/json_compiling.js";
import fs from "fs";
import { loadFromFile }from "../src/file_io.js";
const _ = require('lodash');

test('compileAndSaveEndpoints should save backup of fetched json files', async () => {
  const fileName = "users.json";
  const path = BASEPATH + "/" + fileName;
  if (fs.existsSync(path)) {
    fs.unlinkSync(path);
  }

  await compileAndSaveEndpoints();

  expect(fs.existsSync(path)).toEqual(true);

  const users = loadFromFile(BASEPATH, fileName);
  const id = "-MYaJYUrII35RcHQsSIB"
  const expected = {"name": "P4ul"};
  expect(users[id]).toEqual(expected);
});

test('mergeIgnoringNull should merge objects, ignoring null/undefined fields', () => {
  const obj1 = { name: "noName", age: null, hobby: "breathing" }
  const obj2 = { name: "aName", city: "aCity", job: undefined }
  const actual = mergeIgnoringNull({}, obj1, obj2)
  const expected = { name: "aName", city: "aCity", hobby: "breathing" }

  expect(_.isEqual(actual, expected)).toEqual(true);
});

test('capitalizeFirstLetter should capitalize first letter', () => {
  const beforeSanitation = "jeAn-lUc";
  const expected = "Jean-Luc"

  expect(capitalizeFirstLetter(beforeSanitation)).toEqual(expected);
});

test('sanitizeName should sanitize name', () => {
  const beforeSanitation = "l43t1T140";
  const expected = "Laetitiao"

  expect(sanitizeName(beforeSanitation)).toEqual(expected);
});

test('compileAndSaveEndpoints sould be deep equal to the object in results.json', async () => {
  const actual = await compileAndSaveEndpoints();
  const expected = loadFromFile("./test/resources", "results.json");

  expect(_.isEqual(actual, expected)).toEqual(true);
});