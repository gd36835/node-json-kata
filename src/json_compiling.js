import { loadData, uploadData } from "./api/api.js";
import { saveToFile } from "./file_io.js";

const INPUTBASEURL = "https://recrutement-practice-default-rtdb.firebaseio.com/";
const OUTPUTBASEURL = "https://api.jsonbin.io/v3/b";
export const BASEPATH = "tmp";

export function mergeIgnoringNull(...objects) {
    let result = {}
    objects.forEach(object => {
        for (const key in object) {
            if (object[key] !== undefined && object[key] !== null) {
                result[key] = object[key];
            }
        }
    });
    return result
}

export function capitalizeFirstLetter(string) {
    return string.split('-')
        .map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
        .join('-');
}

export function sanitizeName(name) {
    const sanitizedName = name
        .replaceAll('3', 'e')
        .replaceAll('4', 'a')
        .replaceAll('1', 'i')
        .replaceAll('0', 'o');
    return capitalizeFirstLetter(sanitizedName);
}

export function compileAndSanitizeUsers(users, jobs, infos) {
    let compiledUsers = { ...users };

    for (const key of Object.keys(users)) {
        // remove error names from users
        if (compiledUsers[key].name == "#ERROR") {
            delete compiledUsers[key].name;
        }

        // add cities from informations.json
        if (key in infos && "age" in infos[key]) {
            compiledUsers[key].age = infos[key].age;
        }

        // merge the other attributes without overwriting names and cities, ignore null and undefined fields
        compiledUsers[key] = mergeIgnoringNull({}, jobs[key], infos[key], compiledUsers[key]);
    }

    // sanitize all names and cities
    for (const object of Object.values(compiledUsers)) {
        object.name = sanitizeName(object.name);
        if ("city" in object) {
            object.city = capitalizeFirstLetter(object.city);
        }
    }
    return compiledUsers;
}

export async function compileAndSaveEndpoints() {

    const endpoints = ["informations", "jobs", "users"];

    // Fetch json files
    const [informations, jobs, users] = await Promise.all(endpoints.map(async (endpoint) => {
        const fetchedData = await loadData(INPUTBASEURL, endpoint + ".json");

        // Save the backups in BASEPATH folder
        saveToFile(BASEPATH, endpoint + ".json", JSON.stringify(fetchedData, null, 2));

        return fetchedData;
    }));

    // compile the 3 objects into 1 object
    const compiledUsers = compileAndSanitizeUsers(users, jobs, informations);

    // save backup of compiled object
    saveToFile(BASEPATH, "compiled-users.json", JSON.stringify(compiledUsers, null, 2));

    return compiledUsers;
}

export function uploadJSON(data) {
    // upload compiled object to jsonbin and display read URL
    uploadData(OUTPUTBASEURL, data).then(id => {
        console.log("File successfully uploaded to : " + OUTPUTBASEURL + "/" + id)
    });
}
