import axios from "axios";

export async function loadData(baseURL, fileName) {
    return axios.get(baseURL + fileName)
        .catch(function (error) {
            if (error.response) {
                // Request made and server responded
                throw new Error("Error : Could not load data from endpoint, response status :" + error.response.status)
            } else if (error.request) {
                // Request made but no response was received
                throw new Error("Error : Could not load data from endpoint : no response from server");
            }
        })
        .then(r => r.data);
}

export async function uploadData(baseURL, data) {
    const headers = {
        "X-Master-Key": process.env.JSONBIN_API_KEY,
        "Content-Type": "application/json",
        "X-Bin-Private": false
    }

    return axios.post(baseURL, data, { headers: headers })
        .then((response) => {
            return response.data.metadata.id;
        })
        .catch((e) => {
            throw new Error("Error : Could not upload data: " + e.response.data.message)
        })
}
