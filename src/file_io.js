import fs from 'fs';

export function loadFromFile(parentPath, fileName) {
    let rawData = fs.readFileSync(parentPath + '/' + fileName);
    return JSON.parse(rawData);
}

export function saveToFile(parentPath, fileName, data) {
    const path = parentPath + '/' + fileName;
    // create the directory if it does not exist
    fs.mkdirSync(parentPath, { recursive: true })

    // write data as a new file, or overwrites if it exists
    fs.writeFileSync(path, data, function (err) {
        if (err) throw err;
    });
    return path;
}
