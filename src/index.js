import { compileAndSaveEndpoints, uploadJSON } from "./json_compiling.js";
import "dotenv/config.js"

const compiledUsers = await compileAndSaveEndpoints();

uploadJSON(JSON.stringify(compiledUsers, null, 2));